package com.itglance.springboot.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(method = RequestMethod.GET, value = "employees")
    public List<Employee> getAllEmployee(){
        return employeeService.getAllEmployee();
    }

    @RequestMapping(method = RequestMethod.GET, value = "employees/{id}")
    public Employee getEmployee(@PathVariable("id") int id){
        return employeeService.getEmployee(id);
    }

    @RequestMapping(method=RequestMethod.POST, value = "/employees")
    public void addEmployee(@RequestBody Employee employee) {
        employeeService.addEmployee(employee);
    }

    @RequestMapping(method=RequestMethod.PUT, value = "/employees/{id}")
    public void updateEmployee(@PathVariable("id") int id, @RequestBody Employee employee) {
        employeeService.update(id, employee);
    }

    @RequestMapping(method=RequestMethod.DELETE, value = "/employees/{id}")
    public void updateEmployee(@PathVariable("id") int id) {
        employeeService.delete(id);
    }
}
