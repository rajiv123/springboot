package com.itglance.springboot.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class EmployeeService {

    private List<Employee> employeeList = new ArrayList<>(Arrays.asList(
            new Employee(1, "Ram", "Lal", 90000),
            new Employee(2, "Gabbar", "Singh", 190000),
            new Employee(3, "Jay", "Viru", 9000)
    ));

    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> getAllEmployee() {
        List<Employee> employeeList = new ArrayList<>();
        employeeRepository.findAll().forEach(employeeList::add);
        return employeeList;
    }

    public Employee getEmployee(int id) {
        return employeeRepository.findById(id).get();
    }

    public void addEmployee(Employee employee) {
        employeeRepository.save(employee);
    }

    public void update(int id, Employee employee) {
        employeeRepository.save(employee);
    }

    public void delete(int id) {
        employeeRepository.deleteById(id);
    }
}
